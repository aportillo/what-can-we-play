# What Can We Play

Find out which Steam library games you and your friends have in common.

## Usage

URL

For a quick demonstration, click the 'try me' button. It will auto-populate steam profiles and load a list of common games in all users' libraries.

The Steam API will only share a user's game library if they have set their game details to public in their privacy settings. To change privacy settings:

-   visit your [Steam profile page](http://steamcommunity.com/my/profile)
-   click 'Edit Profile'
-   click 'My Privacy Settings'
-   set 'Game Details' to public


## Built With


## Install
