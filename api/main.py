"""
What Can We Play
Steam API wrapper returns user's game library and common games
when multiple users specified.
"""
from json.decoder import JSONDecodeError
import json
import os

from flask import Flask, jsonify, Response
from flask_cors import CORS
import requests

STEAM_API_KEY = os.environ["STEAM_API_KEY"]

app = Flask(__name__)
CORS(app)

# TODO - restrict to "https://www.games.alexportillo.com"


@app.route("/health")
def health():
    """Test endpoint."""
    return "ok"


@app.route("/sample")
def get_sample_profile() -> dict:
    """Return sample profile data."""
    with open("test_data.json") as f:
        return json.load(f)


@app.route("/profile/<vanity_id>")
def get_user_summary_full(vanity_id: str = None) -> dict:
    """Return user's summary, including games vanity ID.

    Args:
        vanity_id (str, optional): Vanity ID. Defaults to
        None.

    Returns:
        dict: Profile details.
    """
    # Convert vanity ID to Steam ID.
    steam_id, status = get_user_id(vanity_id)
    if steam_id.get("error"):
        return steam_id, status
    else:
        steam_id = steam_id["steamid"]

    # Get profile summary from Steam ID.
    summary, status = get_user_summary(steam_id)
    if summary.get("error"):
        return summary, status

    games, status = get_games(steam_id)
    if games.get("error"):
        return games, status
    else:
        summary.update(games)
        summary["friends"] = []  # front end requires empty friends array

    return summary


@app.route("/summary/<user_id>")
def get_user_summary(user_id: int = None) -> dict:
    """Return user's profile summary, including avatar url.

    Args:
        user_id (int, optional): Steam id in 'steamID64' format. Defaults to
        None.

    Returns:
        dict: Profile details.
    """

    url = (
        f"http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002"
        f"?key={STEAM_API_KEY}&steamids={user_id}&format=json"
    )

    steam_response = requests.get(url)

    if steam_response.status_code == 200:
        steam_response_body = steam_response.json()["response"]

        if len(steam_response_body["players"]) > 0:
            return steam_response_body["players"][0], 200
        else:
            return {"error": "User not found."}, 404

    else:
        return {"error": "Unknown error."}, 500


@app.route("/games/<user_id>")
def get_games(user_id: int = None) -> dict:
    """Return all games in user's library.

    Args:
        user_id (int, optional): Steam ID in steamID64 format. Defaults
        to None.

    Returns:
        dict: All games in user's library.
    """
    if user_id is None:
        return {"error": "no steam user id provided"}, 400

    url = (
        f"http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/"
        f"?key={STEAM_API_KEY}&steamid={user_id}&format=json&include_appinfo=1"
    )
    steam_response = requests.get(url)

    if steam_response.status_code == 200:
        steam_response_body = steam_response.json()
        if steam_response_body["response"].get("game_count"):
            return steam_response_body["response"], 200
        else:
            return {"error": "User not found."}, 404

    if steam_response.status_code == 500:
        # steam returns a 500 when user id not in correct format
        return {"error": "Malformed user ID."}, 400

    else:
        return {"error": "Unknown error."}, 500


@app.route("/id/<vanity_id>")
def get_user_id(vanity_id: str = None) -> dict:
    """Return Steam ID from 'vanity' ID.

    Args:
        vanity_id (str, optional): Steam user's vanity ID, as seen in their
        profile. Defaults to None.

    Returns:
        dict: Steam user's ID in 'steamID64' format.
    """
    url = (
        f"https://api.steampowered.com/ISteamUser/ResolveVanityURL/v1/"
        f"?key={STEAM_API_KEY}&vanityurl={vanity_id}&format=json"
    )
    steam_response = requests.get(url)

    if steam_response.status_code == 200:
        steam_response_body = steam_response.json()["response"]
        if steam_response_body.get("success") == 1:
            return steam_response_body, 200
        else:
            return {"error": "User not found."}, 404
    else:
        return {"error": "Unknown error."}, 500


@app.route("/friends/<user_id>")
def get_friends(user_id: int = None) -> dict:
    """Return user's friends list.

    Args:
        user_id (int, optional): Steam ID in 'steamID64' format. Defaults to
        None.

    Returns:
        dict: Friends list.
    """

    url = (
        f"http://api.steampowered.com/ISteamUser/GetFriendList/v0001/"
        f"?key={STEAM_API_KEY}&steamid={user_id}&relationship=friend"
    )

    steam_response = requests.get(url)

    try:
        steam_response_body = steam_response.json()
    except JSONDecodeError:
        if steam_response.status_code == 500:
            # steam returns a 500 when user id not in correct format
            return {"error": "Malformed user ID."}, 400
        else:
            return {"error": "Unknown error."}, 500

    if steam_response.status_code == 200 and steam_response_body.get("friendslist"):
        friends = get_friends_profiles(steam_response_body)
        return {"friends": friends}

    elif not bool(steam_response_body):
        return {"error": "User's friends list likely set to private."}, 404

    else:
        return {"error": "Unknown error."}, 500


def get_friends_profiles(friends):
    """Iterate through friends list and add individual friend summary.

    Args:
        friends (dict): List of friends.
    """
    friends = friends["friendslist"]["friends"]

    # Get all friends' profile pictures
    for index, friend in enumerate(friends):
        friend_summary, status = get_user_summary(friend["steamid"])
        if friend_summary.get("error"):
            pass
        friends[index].update(friend_summary)
    return friends


if __name__ == "__main__":
    app.run(host="0.0.0.0", port="8000")
